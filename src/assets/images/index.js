import google from './google.png'
import facebook from './facebook.png'
import logo from './logo.png'
import lock from './lock.png'
import mail from './mail.png'

export const img = {
  google,
  facebook,
  logo,
  lock,
  mail
}