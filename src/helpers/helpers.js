
export const isEmailValid = (value) => {
  const EMAIL_REGEXP = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
  
  return EMAIL_REGEXP.test(value)
}