import { createSlice } from '@reduxjs/toolkit'


const initialState = {
  isAuth: false
}


export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setAuth: (state, { payload }) => {
      state.isAuth = payload
    },
  },
  
})

export const {
  setAuth,
} = authSlice.actions
export default authSlice.reducer
