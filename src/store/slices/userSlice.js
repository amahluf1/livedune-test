import { createSlice } from '@reduxjs/toolkit'


const initialState = {
  user: {
    name: '',
    email: ''
  },
}


export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, { payload }) => {
      state.user.name = payload.name
      state.user.email = payload.email
    },
  },
  
})

export const {
  setUser,
} = userSlice.actions
export default userSlice.reducer
