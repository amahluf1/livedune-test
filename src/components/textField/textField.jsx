import React from 'react'

import './textField.scss'

const TextField = ({
                     className,
                     onChange,
                     onBlur,
                     defaultValue,
                     type = 'text',
                     placeholder,
                     inputProps,
                     name,
                     id,
                     error,
                   }) => {
  const onChangeHandler = (e) => {
    onChange(e)
  }
  const onBlurHandler = (e) => {
    onBlur(e)
  }
  
  const setTextFieldClassName = () => {
    let defaultTextFieldClassName = `text-field ${className}`
    
    if (error) defaultTextFieldClassName += ` text-field--invalid`
    
    return defaultTextFieldClassName
  }
  
  return (
    <div className={setTextFieldClassName()}>
      <input
        className={`text-field__input ${className}__input`}
        onChange={(e) => onChangeHandler(e)}
        onBlur={(e) => onBlurHandler(e)}
        type={type}
        placeholder={placeholder}
        name={name}
        id={id}
        {...inputProps}
        defaultValue={defaultValue}
      />
      {
        error &&
        <div
          className={`text-field__error-message ${className}__error-message`}
          style={{display: typeof error === 'boolean' && 'none'}}
        >
          <span>{error}</span>
        </div>
      }
    </div>
  )
}

export { TextField }