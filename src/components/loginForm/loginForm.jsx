import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

import './loginForm.scss'

import { TextField } from '../textField'
import { Button } from '../button'

const LoginForm = () => {
  const [error, setError] = useState('')
  const [formData, setFormData] = useState({
    'email': '',
    'password': ''
  })
  const navigation = useNavigate()
  
  const onSubmitHandler = () => {
    const { email, password } = formData
    
    if (!email || !password) {
      setError('Введите email, Введите пароль')
    } else {
      if (loginFormValidation(email, password)) {
        navigation('/lk')
      } else {
        setError('Неверный email или пароль')
      }
    }
  }
  
  const loginFormValidation = (email, password) => {
    let isValid = false
    
    if (email === 'example@example.com' || password === 'password2021') {
      isValid = true
    }
    return isValid
  }
  
  const onTextFieldChange = (e) => {
    let { name, value } = e.target
    
    setFormData(prev => ({ ...prev, [name]: value }))
  }
  
  return (
    <form
      className='login-form'
      onSubmit={(e) => e.preventDefault()}
    >
      <TextField
        name='email'
        placeholder='Email'
        onChange={onTextFieldChange}
        error={error && true}
      />
      <TextField
        name='password'
        placeholder='Пароль'
        onChange={onTextFieldChange}
        error={error && true}
      />
      {
        error &&
        <div className='login-form__error-message'>
          {error}
        </div>
      }
      <Button className='login-form__login-btn' onClick={onSubmitHandler}>
        Войти в аккаунт
      </Button>
      <Button
        variant='clean'
        role='link'
        to='/forget_password'
        className='login-form__forget-password-btn'
      >
        Забыли пароль?
      </Button>
    
    </form>
  )
}

export { LoginForm }