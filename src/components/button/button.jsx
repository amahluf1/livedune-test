import React from 'react'

import { NavLink } from 'react-router-dom'

import './button.scss'

import { Loader } from '../loader'

const Button = ({
                  children,
                  onClick,
                  icon,
                  className = '',
                  variant,
                  to,
                  role,
                  maxWidth,
                  disabled,
                  load
                }) => {
  
  const onClickHandler = () => {
    onClick()
  }
  
  const setBtnClassName = () => {
    let defaultBtnClassName = `btn btn--${variant} ${className}`
    
    if (icon) defaultBtnClassName += ' btn--with-icon'
    else if (maxWidth) defaultBtnClassName += ' btn--max-width'
    
    return defaultBtnClassName
  }
  
  return (
    <button
      className={setBtnClassName()}
      onClick={onClickHandler}
      disabled={disabled}
    >
      {icon &&
        <div className={`btn__icon ${className}__icon`}>
          <img src={icon} alt=''/>
        </div>
      }
      {
        role === 'link' ?
          <NavLink
            className={`btn__link ${className}__link`}
            to={to}
          >
            {children}
          </NavLink> :
          <>
            {load &&
              <div className={`btn__loader ${className}__loader`}>
                <Loader/>
              </div>
            }
            <div className={`btn__content ${className}__content`}>
              {children}
            </div>
          </>
      }
    </button>
  )
}

export { Button }