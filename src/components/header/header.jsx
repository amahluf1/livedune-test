import React, { useEffect } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import './header.scss'

import { Button } from '../button'

import { img as images } from '../../assets/images'

import { setAuth } from '../../store/slices/authSlice'


const Header = () => {
  const { isAuth } = useSelector(state => state.auth)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  
  const logout = () => {
    dispatch(setAuth(false))
    localStorage.removeItem('auth')
    navigate('/')
  }
  
  useEffect(() => {
    const auth = localStorage.getItem('auth')
    
    if (auth === 'true') dispatch(setAuth(true))
  }, [])
  
  return (
    <header className='header'>
      <div className='header__logo'>
        <NavLink to='/'>
          <img src={images.logo} alt='livedune'/>
        </NavLink>
      </div>
      <div className='auth-controls'>
        {
          isAuth ?
            <Button
              variant='clean'
              className='auth-controls__logout-btn'
              onClick={logout}
            >
              Выйти
            </Button> :
            <>
              <span className='auth-controls__helper-text'>У вас нет аккаунта?</span>
              <Button
                variant='filled'
                className='auth-controls__btn'
                role='link'
                to='registration'
              >
                Регистрация
              </Button>
            </>
        }
      </div>
    </header>
  )
}

export { Header }