import React from 'react'

import './socialAuth.scss'

import { Button } from '../button'

import { img } from '../../assets/images'

const SocialAuth = () => {
  return (
    <div className='social-auth'>
      <Button icon={img.facebook} className='social-auth__btn'>
        Войти через Facebook
      </Button>
      <Button icon={img.google} className='social-auth__btn'>
        Войти через Google
      </Button>
    </div>
  )
}

export { SocialAuth }