import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import './registrationForm.scss'

import { TextField } from '../textField'
import { Button } from '../button'

import { isEmailValid } from '../../helpers/helpers'

import { setUser } from '../../store/slices/userSlice'
import { setAuth } from '../../store/slices/authSlice'


const RegistrationForm = () => {
  const [error, setError] = useState('')
  const [isPromo, setIsPromo] = useState(false)
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    promoCode: ''
  })
  const navigation = useNavigate()
  const dispatch = useDispatch()
  
  const onSubmitHandler = () => {
    const { name, email } = formData
    
    dispatch(setUser({ name, email }))
    dispatch(setAuth(true))
    
    localStorage.setItem('auth', 'true')
    
    navigation(`/confirm_email/${name}/${email}`)
  }
  
  const onTextFieldChange = (e) => {
    let { name, value } = e.target
    
    setFormData(prev => ({ ...prev, [name]: value }))
  }
  
  const checkEmail = (e) => {
    let { value } = e.target
    
    if (value && !isEmailValid(value)) {
      setError('Возможно вы ошиблись в указании почтового сервиса')
    } else setError('')
  }
  
  const isSubmitDisabled = () => {
    const { name, email, password } = formData
    
    if (name && email && password && !error) {
      return false
    } else return true
  }
  
  return (
    <form
      className='registration-form'
      onSubmit={(e) => e.preventDefault()}
    >
      <TextField
        name='name'
        placeholder='Имя'
        onChange={onTextFieldChange}
      />
      <TextField
        name='email'
        placeholder='Email'
        onChange={onTextFieldChange}
        onBlur={checkEmail}
        error={error}
      />
      <TextField
        name='password'
        placeholder='Пароль'
        onChange={onTextFieldChange}
      />
      {isPromo &&
        <TextField
          name='promo'
          placeholder='Промокод'
          onChange={onTextFieldChange}
        />
      }
      {!isPromo &&
        <Button
          variant='clean'
          className='registration-form__promo-btn'
          onClick={() => setIsPromo(true)}
        >
          У меня есть промокод
        </Button>
      }
      <Button
        variant='filled'
        maxWidth
        className='registration-form__btn'
        onClick={onSubmitHandler}
        disabled={isSubmitDisabled()}
      >
        Создать аккаунт
      </Button>
    </form>
  )
}

export { RegistrationForm }