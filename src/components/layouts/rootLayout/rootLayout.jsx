import React from 'react'

import './rootLayout.scss'

import { Header } from '../../header'

const RootLayout = ({ children }) => {
  return (
    <div className='root-layout'>
      <Header/>
      <div className='page'>
        {children}
      </div>
    </div>
  )
}

export { RootLayout }