import React from 'react'
import { Route, Routes } from 'react-router-dom'

import { Registration } from '../../pages/registration'
import { ForgetPassword } from '../../pages/forgetPassword'
import { Login } from '../../pages/login'
import { ConfirmEmail } from '../../pages/confirmEmail'


const AppRoutes = () => {
  return (
    <Routes>
      <Route path='/registration' element={<Registration/>}/>
      <Route index element={<Login/>}/>
      <Route path='/forget_password' element={<ForgetPassword/>}/>
      <Route path='confirm_email/:name/:email' element={<ConfirmEmail/>}/>
    </Routes>
  )
}

export { AppRoutes }