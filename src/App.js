import './styles/App.scss'
import { RootLayout } from './components/layouts/rootLayout'
import { AppRoutes } from './components/routes'


function App() {
  return (
    <div className='App'>
      <RootLayout>
        <AppRoutes/>
      </RootLayout>
    </div>
  )
}

export default App
