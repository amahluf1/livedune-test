import React, { useState } from 'react'

import './forgetPassword.scss'

import { TextField } from '../../components/textField'
import { Button } from '../../components/button'

import { img } from '../../assets/images'


const ForgetPassword = () => {
  const [loading, setLoading] = useState(false)
  const [isMailSend, setIsMailSend] = useState(false)
  
  const sendPasswordHandler = () => {
    setLoading(true)
    
    setTimeout(() => {
      setLoading(false)
      setIsMailSend(true)
    }, 1000)
  }
  
  return (
    <div className='forget-password fade'>
      <div className='forget-password__img'>
        <img src={isMailSend ? img.mail : img.lock} alt='lock'/>
      </div>
      <div className='forget-password__title page__title'>
        {
          isMailSend ?
            <span>Письмо отправлено</span> :
            <span>Восстановить пароль</span>
        }
      </div>
      <div className='forget-password__subtitle page__subtitle'>
        {
          isMailSend ?
            <span>
              На указанный вами e-mail было отправлено<br/>
              письмо для смены пароля
            </span> :
            <span>Введите e-mail, на который регистрировались ранее</span>
        }
      </div>
      {
        isMailSend ?
          <Button
            variant='filled'
            maxWidth role='link'
            to='/'
            className='forget-password__go-to-main-btn'
          >
            Вернуться на главную
          </Button> :
          <>
            <TextField defaultValue='example@example.com'/>
            <Button
              variant='filled'
              maxWidth
              className='forget-password__send-mail-btn'
              onClick={sendPasswordHandler}
              load={loading}
            >
              {loading ? 'Отправка' : 'Отправить'}
            </Button>
            <Button
              variant='clean'
              className='forget-password__cancel-btn'
              role='link'
              to='/'
            >
              Отменить
            </Button>
          </>
      }
    </div>
  )
}

export { ForgetPassword }