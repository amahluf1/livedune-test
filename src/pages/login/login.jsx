import React from 'react'

import './login.scss'

import { LoginForm } from '../../components/loginForm'
import { SocialAuth } from '../../components/social_auth'


const Login = () => {
  return (
    <div className='login fade'>
      <div className='login__title page__title'>
        <span>Войти</span>
      </div>
      <div className='login__subtitle page__subtitle'>
        <span>Добро пожаловать, рады видеть вас снова 👋</span>
      </div>
      <SocialAuth/>
      <div className='login__pretext page__pretext'>
        <span>или</span>
      </div>
      <LoginForm/>
    </div>
  )
}

export { Login }