import React, { useState } from 'react'
import { useParams } from 'react-router-dom'

import './confirmEmail.scss'

import { Button } from '../../components/button'
import { TextField } from '../../components/textField'

const ConfirmEmail = () => {
  const [isMailReceived, setIsMailReceived] = useState(true)
  
  const params = useParams()
  const { name, email } = params
  
  return (
    <div className='confirm-email fade'>
      <div className='confirm-email__title page__title'>
        {
          isMailReceived ?
            <span>Подтвердите ваш e-mail</span> :
            <span>Мне не пришло письмо</span>
        }
      </div>
      <div className='confirm-email__sub-title page__subtitle'>
        {
          isMailReceived ?
            <p>
              {name}, на ваш E-mail отправлено письмо со ссылкой для <br/>
              подтверждения. Перейдите по ней, чтобы активировать вашу учетную <br/>
              запись и получить 7 дней бесплатного доступа.
            </p> :
            <p>
              Письмо может прийти с задержкой в 5-10 минут.<br/>
              Также проверьте разные папки почтового ящика (актуально для gmail.com) и папку<br/>
              "Спам".Если письмо все же не пришло, повторите попытку или напишите об этом в<br/>
              тех.поддержку <a href='mailto:support@livedune.ru'>support@livedune.ru</a> и мы активируем ваш аккаунт.
            </p>
        }
      </div>
      {
        isMailReceived ?
          <div className='confirm-email__mail-btn'>
            <Button variant='filled'>Перейти к почте</Button>
            <Button variant='clean' onClick={() => setIsMailReceived(false)}>Мне не пришло письмо</Button>
          </div> :
          <div className='confirm-email__resend-mail'>
            <TextField defaultValue={email}/>
            <Button variant='filled'>Отправить заново</Button>
            <Button
              variant='clean'
              onClick={()=> setIsMailReceived(true)}
              className='confirm-email__resend-mail__cancel-btn'
            >
              Отменить
            </Button>
          </div>
      }
    
    </div>
  )
}

export { ConfirmEmail }