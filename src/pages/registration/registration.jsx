import React from 'react'

import './registration.scss'

import { RegistrationForm } from '../../components/registrationForm'
import { SocialAuth } from '../../components/social_auth'

const Registration = () => {
  return (
    <div className='registration fade'>
      <div className='registration__title page__title'>
        <span>Регистрация</span>
      </div>
      <div className='registration__subtitle page__subtitle'>
        <span>Зарегистрируйся и получи доступ к аналитике аккаунтов.</span>
      </div>
      <SocialAuth/>
      <div className='registration__pretext page__pretext'>
        <span>или</span>
      </div>
      <RegistrationForm/>
      <div className='registration__user-terms'>
        <span>Создавая аккаунт, я согласен с <a href='#' >условиями оферты</a></span>
      </div>
    </div>
  )
}

export { Registration }